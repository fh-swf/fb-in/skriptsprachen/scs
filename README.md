# Praktikumsaufgaben
Dieses Repository enthält die *öffentlichen* Versionen der Praktikumsaufgaben zum Modul Skriptsprachen.

Bitte bearbeiten Sie diese Aufgaben über den "offiziellen" Link in Moodle, da Sie aktuell nur so Ihre Lösungen abgeben können.

